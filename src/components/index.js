import React, {useEffect, useRef} from 'react';
import P5 from 'p5';
import sketch from './sketch';
import './index.css';

function App() {
	const boardEl = useRef(null);

	useEffect(() => {
		const p5 = new P5(sketch, boardEl.current);
		return () => p5.remove();
	});

	return (
		<div className="App">
			<div className="App-board" ref={boardEl} />
		</div>
	);
}

export default App;
