import 'p5/lib/addons/p5.sound';

const NUM_WIDTH_CELL = 7;
const NUM_HEIGHT_CELL = 6;
const DIAMETER_MULT = 0.8;

class Board {
	constructor() {
		this.cellSize = this.getCellSize();
		// Diameter of circle 80% => radii 40%
		this.circleDiameter = this.cellSize * DIAMETER_MULT;
		this.width = this.cellSize * NUM_WIDTH_CELL;
		this.height = this.cellSize * NUM_HEIGHT_CELL;
	}

	getCellSize() {
		const {innerWidth, innerHeight} = window;

		// How much of the screen should the board take
		let sizeModifier = 0.8;
		if (innerWidth < innerHeight) {
			sizeModifier = 0.95;
		}
		const width = innerWidth * sizeModifier;
		const height = innerHeight * sizeModifier;

		// Cell and circle
		const cellWidth = Math.floor(width / NUM_WIDTH_CELL);
		const cellHeight = Math.floor(height / NUM_HEIGHT_CELL);
		return cellWidth < cellHeight ? cellWidth : cellHeight;
	}

	render(p5) {
		p5.createCanvas(this.width, this.height);

		this.renderGrid(p5);
		this.renderCircles(p5);
	}

	renderGrid(p5) {
		// TODO get values from koji
		const gridColor = [0, 72, 186];
		// draw board
		p5.fill(...gridColor);
		p5.rect(0, 0, this.width, this.height);
	}

	renderCircles(p5) {
		p5.fill(255);

		// TODO remove
		const length = NUM_WIDTH_CELL * NUM_HEIGHT_CELL;
		for (let i = 0; i < length; i++) {
			const x = (Math.floor(i / NUM_HEIGHT_CELL) + 0.5) * this.cellSize;
			const y = ((i % NUM_HEIGHT_CELL) + 0.5) * this.cellSize;
			p5.circle(x, y, this.circleDiameter);
		}
	}
}

function sketch(p5) {
	p5.setup = () => {
		const board = new Board();
		board.render(p5);
	};

	p5.draw = () => {
		// if (p5.mouseIsPressed) {
		// 	p5.fill(0);
		// } else {
		// 	const color = p5.color(65, 105, 225);
		// 	p5.fill(color);
		// }
		// p5.ellipse(p5.mouseX, p5.mouseY, 80, 80);
	};
}

export default sketch;
